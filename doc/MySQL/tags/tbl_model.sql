
CREATE TABLE `tbl_model` (
`id` bigint(20) DEFAULT NULL,
`tag_id` bigint(20) DEFAULT NULL COMMENT '标签ID',
`type` int(11) DEFAULT NULL COMMENT '算法类型：统计-Statistics、规则匹配-Match、挖掘-具体算法-DecisionTree',
`model_name` varchar(200) DEFAULT NULL COMMENT '模型名称',
`model_main` varchar(200) DEFAULT NULL COMMENT '模型运行主类名称',
`model_path` varchar(200) DEFAULT NULL COMMENT '模型JAR包HDFS路径',
`sche_time` varchar(200) DEFAULT NULL COMMENT '模型调度时间',
`ctime` datetime DEFAULT NULL COMMENT '创建模型时间戳',
`utime` datetime DEFAULT NULL COMMENT '更新模型时间戳',
`state` int(11) DEFAULT NULL COMMENT '模型状态，1：运行；0：停止',
`remark` varchar(100) DEFAULT NULL,
`operator` varchar(100) DEFAULT NULL,
`operation` varchar(100) DEFAULT NULL,
`args` varchar(100) DEFAULT NULL COMMENT '模型运行应用配置参数，如资源配置参数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `tbl_model` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`tag_id` bigint(20) DEFAULT NULL,
`model_name` varchar(200) DEFAULT NULL,
`model_main` varchar(200) DEFAULT NULL,
`model_path` varchar(200) DEFAULT NULL,
`sche_time` varchar(200) DEFAULT NULL,
`ctime` datetime DEFAULT NULL,
`utime` datetime DEFAULT NULL,
`state` int(11) DEFAULT NULL,
`args` varchar(100) DEFAULT NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;